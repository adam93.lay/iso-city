﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Models;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class MouseController : MonoBehaviour
{
  public Text TextHoverTarget;
  public GameObject HoverTooltip;

  private Vector3 _lastFramePosition;
  private Vector3 _currFramePosition;
  private Vector3 _dragStartPosition;
  private Tile _lastHoverTarget;
  private GameObject _lastHoverTargetGo;
  private bool _isDragging;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    _currFramePosition = GetMousePosition();

    UpdateLeftClick();
    UpdateCamera();

    // Get new position after camera move
    _lastFramePosition = GetMousePosition();
  }

  private void UpdateLeftClick()
  {
    // Start drag
    if (Input.GetMouseButtonDown((int)MouseButton.LeftMouse) && !EventSystem.current.IsPointerOverGameObject())
    {
      _isDragging = true;
      _dragStartPosition = _currFramePosition;
    }

    //int dragX = Mathf.FloorToInt(_dragStartPosition.x);
    //int currentX = Mathf.FloorToInt(_currFramePosition.x);
    //int dragY = Mathf.FloorToInt(_dragStartPosition.y);
    //int currentY = Mathf.FloorToInt(_currFramePosition.y);

    //int startX = Math.Min(dragX, currentX);
    //int endX = Math.Max(dragX, currentX);
    //int startY = Math.Min(dragY, currentY);
    //int endY = Math.Max(dragY, currentY);

    //for (int i = _dragPreviews.Count - 1; i >= 0; i--)
    //{
    //  SimplePool.Despawn(_dragPreviews[i]);
    //  _dragPreviews.RemoveAt(i);
    //}

    //if (Input.GetMouseButton((int)MouseButton.LeftMouse))
    //{
    //  for (int x = startX; x <= endX; x++)
    //  for (int y = startY; y <= endY; y++)
    //  {
    //    Tile t = World.Instance.GetTileAt(x, y);

    //    if (t != null)
    //    {
    //      // Display building hint
    //      GameObject go = SimplePool.Spawn(CursorPrefab, new Vector3(x, y), Quaternion.identity);
    //      go.transform.SetParent(this.transform);
    //      _dragPreviews.Add(go);
    //    }
    //  }
    //}

    GameObject targetGo = null;
    Tile target = null;

    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

    if (hit.collider != null)
    {
      targetGo = hit.collider.gameObject;

      string[] parts = hit.collider.name.Split('_');

      target = World.Instance.GetTileAt(int.Parse(parts[1]), int.Parse(parts[2]));
    }

    if (_lastHoverTargetGo != targetGo)
    {
      const float hoverHeightAdjust = 0.025f;
      // Remove old hover state
      if (_lastHoverTargetGo != null)
      {
        Vector3 newPos = _lastHoverTargetGo.transform.position;
        newPos.y -= hoverHeightAdjust;
        _lastHoverTargetGo.transform.position = newPos;
        var lsr = _lastHoverTargetGo.GetComponent<SpriteRenderer>();

        lsr.sprite = SpriteController.Instance.GetSpriteForTile(_lastHoverTarget);
        lsr.color = Color.white;
      }

      // Add Hover State
      if (targetGo != null)
      {
        Vector3 newPos = targetGo.transform.position;
        newPos.y += hoverHeightAdjust;
        targetGo.transform.position = newPos;

        var sr = targetGo.GetComponent<SpriteRenderer>();

        BuildController bc = BuildController.Instance;

        sr.sprite = bc.GetSpriteForPreview(target);
        sr.color = bc.IsPlacementValid(target)
          ? new Color(0.8f, 1f, 0.8f, 0.8f)
          : new Color(1f, 0.8f, 0.8f, 0.8f);

        if (target.Building != null)
        {
          HoverTooltip.SetActive(true);

          var hoverTipRect = HoverTooltip.GetComponent<RectTransform>();
          Vector3 panelPoint = Camera.main.WorldToScreenPoint(targetGo.transform.position);
          float halfPanelWidth = hoverTipRect.rect.width / 2;

          if (panelPoint.x - halfPanelWidth < 0)
            panelPoint.x = halfPanelWidth;
          if (panelPoint.x + halfPanelWidth > Screen.width)
            panelPoint.x = Screen.width - halfPanelWidth;

          HoverTooltip.transform.position = panelPoint;
        }
        else
        {
          HoverTooltip.SetActive(false);
        }
      }
      else
      {
        HoverTooltip.SetActive(false);
      }

      _lastHoverTarget = target;
      _lastHoverTargetGo = targetGo;
    }

    TextHoverTarget.text = "Mouse Over: " + (target == null ? "" : "Tile_" + target.X + "_" + target.Y);

    if (!_isDragging)
      return;

    // End drag
    if (Input.GetMouseButtonUp((int)MouseButton.LeftMouse))
    {
      _isDragging = false;

      BuildController.Instance.Build(target);

      //BuildModeController bmc = BuildModeController.Instance;

      //for (int x = startX; x <= endX; x++)
      //for (int y = startY; y <= endY; y++)
      //{
      //  Tile t = world.GetTileAt(x, y);

      //  if (t != null)
      //  {
      //    bmc.Build(t);
      //  }
      //}

      //RaycastHit2D hit = Physics2D.Raycast();
    }
  }

  private void UpdateCamera()
  {
    if (Input.GetMouseButton((int)MouseButton.MiddleMouse) || Input.GetMouseButton((int)MouseButton.RightMouse))
    {
      Vector3 diff = _lastFramePosition - _currFramePosition;

      Camera.main.transform.Translate(diff);
    }

    Camera.main.orthographicSize -= Camera.main.orthographicSize * Input.GetAxis("Mouse ScrollWheel");

    Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 2f, 25f);
  }

  private Vector3 GetMousePosition()
  {
    Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

    pos.z = 0;

    return pos;
  }
}
