﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Models;
using Assets.Scripts.Models.Buildings;
using Assets.Scripts.Models.Enums;
using UnityEngine;

public class SpriteController : MonoBehaviour
{
  public static SpriteController Instance;

  private Dictionary<Tile, GameObject> _tileGameObjectMap;
  private Dictionary<string, Sprite> _sprites;

  // Use this for initialization
  void Start()
  {
    Instance = this;

    _tileGameObjectMap = new Dictionary<Tile, GameObject>();

    LoadSprites();

    World.Instance.OnTileChanged += OnTileChanged;
    
    World.Instance.ForEachTile(tile =>
    {
      GameObject go = _tileGameObjectMap[tile] = new GameObject { name = "Tile_" + tile.X + "_" + tile.Y };
      
      float x = tile.X * 0.5f - tile.Y * 0.5f;
      float y = tile.Y * 0.25f + tile.X * 0.25f;

      go.transform.position = new Vector3(x, y, y);
      go.transform.parent = this.transform;
      
      var sr = go.AddComponent<SpriteRenderer>();

      sr.sprite = GetSpriteForTile(tile);
      sr.sortingOrder = tile.X * -1 + tile.Y * -1;

      var pc = go.AddComponent<PolygonCollider2D>();
    });
  }

  private void OnTileChanged(Tile tile)
  {
    GameObject go = _tileGameObjectMap[tile];

    var sr = go.GetComponent<SpriteRenderer>();

    sr.sprite = GetSpriteForTile(tile);
  }

  private void LoadSprites()
  {
    _sprites = new Dictionary<string, Sprite>();

    Sprite[] allSprites = Resources.LoadAll<Sprite>("Images");

    _sprites = allSprites.ToDictionary(k => k.name, v => v);
  }

  public Sprite GetSpriteForTile(Tile tile)
  {
    return tile.Building != null
      ? GetSpriteForBuilding(tile.Building, tile)
      : GetSpriteForType(tile.Type, tile);
  }

  public Sprite GetSpriteForType(TileTypes type, Tile tile)
  {
    return GetSprite("Tile_" + type, tile, type.ToString(), type.LinksToNeighbour());
  }

  public Sprite GetSpriteForBuilding(Building b, Tile tile)
  {
    if (b.MustBeBuiltNextTo.Length > 0)
    {
      string spriteName = "Building_" + b.Type + "_";
      string suffix = "W";
      string orientations = "NESW";

      Tile[] neighbours = tile.GetNeighbours();

      for (int i = neighbours.Length - 1; i >= 0; i--)
      {
        Tile n = neighbours[i];

        if (n == null || !b.MustBeBuiltNextTo.Contains(n.Type))
          continue;

        suffix = orientations[i].ToString();
        break;
      }

      return _sprites[spriteName + suffix];
    }

    return _sprites["Building_" + b.Type + "_W"];
  }

  public Sprite GetSprite(string name, Tile tile, string type, bool linksToNeighbour = false)
  {
    if (!linksToNeighbour)
      return _sprites[name];

    string spriteName = name;
    string suffix = "";

    Tile[] neighbours = tile.GetNeighbours();

    if (neighbours[0] != null && neighbours[0].Type.ToString() == type)
      suffix += "N";
    if (neighbours[1] != null && neighbours[1].Type.ToString() == type)
      suffix += "E";
    if (neighbours[2] != null && neighbours[2].Type.ToString() == type)
      suffix += "S";
    if (neighbours[3] != null && neighbours[3].Type.ToString() == type)
      suffix += "W";

    string fullName = spriteName + (suffix == "" ? "" : "_" + suffix);

    if (!_sprites.ContainsKey(fullName))
    {
      Debug.LogError("No sprite for: " + fullName);
      return _sprites[spriteName];
    }

    return _sprites[fullName];
  }
}