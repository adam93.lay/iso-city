﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Models;
using Assets.Scripts.Models.Buildings;
using UnityEngine;

public class WorldController : MonoBehaviour
{
  // Use this for initialization
  void OnEnable()
  {
    var w = new World(7, 7);

    w.MoneyBalance.Add(100);
  }

  // Update is called once per frame
  void Update()
  {
    foreach (Building b in World.Instance.Buildings.Values)
    {
      if (b != null)
        b.Update(Time.deltaTime);
    }
  }
}
