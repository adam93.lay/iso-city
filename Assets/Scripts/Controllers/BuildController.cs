﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Models;
using Assets.Scripts.Models.Buildings;
using Assets.Scripts.Models.Enums;
using UnityEngine;
using UnityEngine.UI;

public class BuildController : MonoBehaviour
{
  public static BuildController Instance;
  public TileTypes? TileMode = TileTypes.Grass;
  public BuildingTypes? BuildingMode;
  public Text TextBuildMode;

  private Dictionary<BuildingTypes, Building> _buildingPrototypes;

  // Use this for initialization
  void Start()
  {
    Instance = this;

    SetModeText();

    Building[] prototypes =
    {
      new Shop
      {
        MoneyToBuild = 50f,
        MustBeBuiltNextTo = new [] { TileTypes.Road, TileTypes.Road_Concrete },
        MustBeBuiltOn = new [] { TileTypes.Concrete }
      },
      new PowerStation
      {
        MoneyToBuild = 50f,
        CannotBeBuiltOn = new [] { TileTypes.Canal, TileTypes.Road, TileTypes.Road_Concrete }
      }
    };

    _buildingPrototypes = prototypes.ToDictionary(b => b.Type, b => b);
  }

  public bool Build(Tile target)
  {
    if (TileMode != null)
    {
      return target.ChangeType(TileMode.Value);
    }

    if (BuildingMode != null)
    {
      if (BuildingMode.Value == BuildingTypes.Bulldoze)
        return target.BulldozeBuilding();

      Building proto = _buildingPrototypes[BuildingMode.Value];

      if (IsPlacementValid(target) && World.Instance.MoneyBalance.Spend(proto.MoneyToBuild))
      {
        return target.Build(Building.FromPrototype(proto));
      }
    }

    return false;
  }

  public void SetTileMode(string mode)
  {
    BuildingMode = null;
    TileMode = (TileTypes)Enum.Parse(typeof(TileTypes), mode);

    SetModeText();
  }

  public void SetBuildingMode(string mode)
  {
    TileMode = null;
    BuildingMode = (BuildingTypes)Enum.Parse(typeof(BuildingTypes), mode);

    SetModeText();
  }

  public bool IsPlacementValid(Tile tile)
  {
    bool hasValidNeighbour = true;
    bool isOnValidTile = true;
    bool canAfford = true;
    bool canBuildOnTile = true;

    if (BuildingMode != null && BuildingMode != BuildingTypes.Bulldoze)
    {
      Building proto = _buildingPrototypes[BuildingMode.Value];

      if (proto.MustBeBuiltOn.Length > 0 || proto.CannotBeBuiltOn.Length > 0)
      {
        isOnValidTile = false;

        if (proto.MustBeBuiltOn.Length > 0 && proto.CannotBeBuiltOn.Length > 0)
        {
          isOnValidTile =
            proto.MustBeBuiltOn.Contains(tile.Type)
            && !proto.CannotBeBuiltOn.Contains(tile.Type);
        }
        else
        {
          if (proto.MustBeBuiltOn.Length > 0)
            isOnValidTile = proto.MustBeBuiltOn.Contains(tile.Type);

          if (proto.CannotBeBuiltOn.Length > 0)
            isOnValidTile = !proto.CannotBeBuiltOn.Contains(tile.Type);
        }
      }

      if (proto.MustBeBuiltNextTo.Length > 0)
      {
        hasValidNeighbour = false;

        foreach (Tile n in tile.GetNeighbours().Where(n => n != null))
        {
          hasValidNeighbour = proto.MustBeBuiltNextTo.Contains(n.Type);

          if (hasValidNeighbour)
            break;
        }
      }

      canAfford = proto.MoneyToBuild <= World.Instance.MoneyBalance.Balance;

      canBuildOnTile = tile.Building == null;
    }

    return hasValidNeighbour && isOnValidTile && canAfford && canBuildOnTile;
  }

  private void SetModeText()
  {
    if (TileMode != null)
    {
      TextBuildMode.text = "Tile Mode: " + TileMode;
    }
    else if (BuildingMode != null)
    {
      TextBuildMode.text = "Building Mode: " + BuildingMode;
    }
  }

  public Sprite GetSpriteForPreview(Tile target)
  {
    SpriteController tc = SpriteController.Instance;

    if (TileMode != null)
      return tc.GetSpriteForType(TileMode.Value, target);

    if (BuildingMode != null && BuildingMode != BuildingTypes.Bulldoze)
      return tc.GetSpriteForBuilding(_buildingPrototypes[BuildingMode.Value], target);

    return null;
  }
}
