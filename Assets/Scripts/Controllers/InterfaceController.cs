﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Models;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceController : MonoBehaviour
{
  public Text TextCurrentMoney;
  public Text TextMoneyIncome;
  public Text TextCurrentPower;
  public Text TextPowerIncome;

  // Use this for initialization
  void Start()
  {
    World world = World.Instance;

    world.MoneyBalance.OnChanged += OnWorldMoneyChanged;
    world.PowerBalance.OnChanged += OnWorldPowerChanged;
    world.OnTileChanged += OnTileChanged;

    OnWorldMoneyChanged(world.MoneyBalance.Balance);
    OnWorldPowerChanged(world.PowerBalance.Balance);
    OnTileChanged(null);
  }

  private void OnWorldPowerChanged(float balance)
  {
    TextCurrentPower.text = "PWR" + balance.ToString("N");
  }

  private void OnTileChanged(Tile tile)
  {
    IncomesModel incomes = World.Instance.GetIncomes();

    TextMoneyIncome.text = "+£" + Mathf.RoundToInt(incomes.Money) + "/s";
    TextPowerIncome.text = "+pwr" + Mathf.RoundToInt(incomes.Power) + "/s";
  }

  private void OnWorldMoneyChanged(float balance)
  {
    TextCurrentMoney.text = "£" + balance.ToString("N");
  }
}
