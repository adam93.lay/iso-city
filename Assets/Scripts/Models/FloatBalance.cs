﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Models
{
  public class FloatBalance
  {
    public Action<float> OnChanged;

    private float _balance;
    public float Balance
    {
      get { return _balance; }
      set
      {
        _balance = value;
       if (OnChanged != null) 
          OnChanged(_balance);
      }
    }

    public void Add(float amount)
    {
      Balance += amount;
    }

    public bool Spend(float amount)
    {
      if (amount > Balance)
        return false;

      Balance -= amount;

      return true;
    }
  }
}
