﻿namespace Assets.Scripts.Models.Enums
{
  public enum TileTypes
  {
    Dirt,
    Grass,
    Concrete,
    Road,
    Road_Concrete,
    Canal
  }

  public static class TileTypesExtensions
  {
    public static bool LinksToNeighbour(this TileTypes type)
    {
      switch (type)
      {
        case TileTypes.Road:
        case TileTypes.Road_Concrete:
        case TileTypes.Canal:
          return true;
      }

      return false;
    }
  }
}
