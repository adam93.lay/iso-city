﻿namespace Assets.Scripts.Models.Enums
{
  public enum Orientations
  {
    North,
    East,
    South,
    West
  }
}
