﻿using System;
using System.Diagnostics;
using Assets.Scripts.Models.Enums;

namespace Assets.Scripts.Models.Buildings
{
  public abstract class Building
  {
    public BuildingTypes Type { get; protected set; }

    public abstract void Update(float deltaTime);

    public float MoneyToBuild;
    public float CollectionTime = 5f;
    protected float CollectionTimeRemaining = 5f;

    public float MoneyIncome { get; protected set; }
    public float PowerIncome { get; protected set; }

    public TileTypes[] MustBeBuiltOn;
    public TileTypes[] CannotBeBuiltOn;
    public TileTypes[] MustBeBuiltNextTo;

    public Building()
    {
      MustBeBuiltOn = new TileTypes[0];
      CannotBeBuiltOn = new TileTypes[0];
      MustBeBuiltNextTo = new TileTypes[0];
    }

    public static Building FromPrototype(Building proto)
    {
      Building result;

      switch (proto.Type)
      {
        case BuildingTypes.Shop:
          result = new Shop();
          break;
        case BuildingTypes.PowerStation:
          result = new PowerStation();
          break;
        default:
          UnityEngine.Debug.LogError("No building type found for: " + proto.Type);
          return null;
      }

      result.Type = proto.Type;
      result.MoneyToBuild = proto.MoneyToBuild;
      result.CollectionTime = result.CollectionTimeRemaining = proto.CollectionTime;
      result.MoneyIncome = proto.MoneyIncome;
      result.PowerIncome = proto.PowerIncome;
      result.MustBeBuiltOn = proto.MustBeBuiltOn;
      result.CannotBeBuiltOn = proto.CannotBeBuiltOn;
      result.MustBeBuiltNextTo = proto.MustBeBuiltNextTo;

      return result;
    }
  }
}
