﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Models.Enums;

namespace Assets.Scripts.Models.Buildings
{
  public class PowerStation : Building
  {
    public PowerStation()
    {
      Type = BuildingTypes.PowerStation;

      PowerIncome = 15f;
    }
    
    public override void Update(float deltaTime)
    {
      if ((CollectionTimeRemaining -= deltaTime) <= 0)
      {
        World.Instance.PowerBalance.Add(PowerIncome);

        CollectionTimeRemaining = CollectionTime;
      }
    }
  }
}
