﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Models.Enums;

namespace Assets.Scripts.Models.Buildings
{
  public class Shop : Building
  {
    public Shop()
    {
      Type = BuildingTypes.Shop;

      MoneyIncome = 15f;
    }
    
    public override void Update(float deltaTime)
    {
      if ((CollectionTimeRemaining -= deltaTime) <= 0)
      {
        World.Instance.MoneyBalance.Add(MoneyIncome);

        CollectionTimeRemaining = CollectionTime;
      }
    }
  }
}
