﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Models;
using Assets.Scripts.Models.Buildings;
using Assets.Scripts.Models.Enums;
using UnityEngine;

namespace Assets.Scripts.Models
{
  public class Tile
  {
    public int X;
    public int Y;
    public int Height;

    private Building _building;

    public Building Building
    {
      get { return _building; }
      private set
      {
        _building = value;
        if (OnTileChanged != null)
          OnTileChanged(this);
      }
    }

    private TileTypes _type;
    public TileTypes Type
    {
      get { return _type; }
      private set
      {
        if (_type != value)
        {
          _type = value;
          if (OnTileChanged != null)
            OnTileChanged(this);
        }
      }
    }

    public Action<Tile> OnTileChanged;


    public Tile[] GetNeighbours()
    {
      World w = World.Instance;

      var results = new Tile[4];

      results[0] = w.GetTileAt(X, Y + 1); // "North"
      results[1] = w.GetTileAt(X + 1, Y); // "East"
      results[2] = w.GetTileAt(X, Y - 1); // "South"
      results[3] = w.GetTileAt(X - 1, Y); // "West"

      return results;
    }

    /// <summary>
    /// Iterate over non null adjacent tiles. Order => NESW.
    /// </summary>
    /// <param name="action"></param>
    public void ForEachNeighbour(Action<Tile> action)
    {
      foreach (Tile t in GetNeighbours().Where(t => t != null))
        action(t);
    }

    /// <summary>
    /// Iterate over non null adjacent tiles. Order => NESW.
    /// </summary>
    /// <param name="action"></param>
    public void ForEachNeighbour(Func<Tile, bool> action)
    {
      foreach (Tile t in GetNeighbours().Where(t => t != null))
        if (action(t))
          return;
    }

    public bool Build(Building b)
    {
      Building = b;

      return true;
    }

    public bool BulldozeBuilding()
    {
      Building = null;
      ChangeType(TileTypes.Dirt);

      return true;
    }

    public bool ChangeType(TileTypes newType)
    {
      if (Building != null)
        return false;

      Type = newType;

      ForEachNeighbour(t =>
      {
        t.OnTileChanged(t);
      });

      return true;
    }
  }
}