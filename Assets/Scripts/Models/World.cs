﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Models.Buildings;

namespace Assets.Scripts.Models
{
  public class World
  {
    public static World Instance;

    public Tile[,] Tiles;
    public Dictionary<Tile, Building> Buildings;

    public int Width;
    public int Height;

    public FloatBalance MoneyBalance;
    public FloatBalance PowerBalance;

    public float MoneyIncome
    {
      get { return Buildings.Values.Where(b => b != null).Sum(b => b.MoneyIncome); }
    }

    public Action<Tile> OnTileChanged;

    public World(int w, int h)
    {
      Width = w;
      Height = h;

      Tiles = new Tile[Width, Height];
      Buildings = new Dictionary<Tile, Building>();

      MoneyBalance = new FloatBalance();
      PowerBalance = new FloatBalance();

      ForEachTile(t => t.OnTileChanged += TileChanged);

      Instance = this;
    }

    private void TileChanged(Tile tile)
    {
      Buildings[tile] = tile.Building;

      if (OnTileChanged != null)
        OnTileChanged(tile);
    }

    // =====
    // Helpers
    // =====

    public void ForEachTile(Action<Tile> action)
    {
      for (int x = 0; x <= Tiles.GetUpperBound(0); x++)
        for (int y = 0; y <= Tiles.GetUpperBound(1); y++)
          action(GetTileAt(x, y));
    }

    public Tile GetTileAt(int x, int y)
    {
      if (x < 0 || y < 0 || x >= Width || y >= Height)
        return null;

      return Tiles[x, y] ?? (Tiles[x, y] = new Tile { X = x, Y = y });
    }

    public IncomesModel GetIncomes()
    {
      var incomes = new IncomesModel();

      foreach (var building in Buildings.Values.Where(b => b != null))
      {
        incomes.Money += building.MoneyIncome / building.CollectionTime;
        incomes.Power += building.PowerIncome / building.CollectionTime;
      }

      return incomes;
    }
  }
}
